User-agent: *
Disallow: /wp-admin/
Disallow: /wp-includes/
Allow: /wp-includes/js/
Allow: /wp-includes/css/
Disallow: /category/
Disallow: /autor/
Disallow: /tag/
Disallow: /page/
Disallow: /feed/
Disallow: /?p=*

Sitemap: http://discesur.es/sitemap_index.xml
