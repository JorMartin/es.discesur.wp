$(document).ready(function() {
	$('.popup-youtube').magnificPopup({

		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});

(function($) {
  "use strict"; // Start of use strict



  window.loadMap = function() {

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 18,
      center: {lat: 40.529685, lng: -3.650066}
    });

    var image = 'http://discesur.betaroiup.com/landings/alcobendas/img/icon-map.png';
    var beachMarker = new google.maps.Marker({
      position: {lat: 40.529685, lng: -3.650066},
      map: map,
      icon: image
    });



    var widgetDiv = document.getElementById('save-widget');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(widgetDiv);

    var saveWidget = new google.maps.SaveWidget(widgetDiv, {
      place: {
        placeId: 'ChIJi7xhMnjjQgwR7KNoB5Qs7KY',
        location: {lat: 40.529685, lng: -3.650066}
      },
      attribution: {
        source: 'Google Maps JavaScript API',
        webUrl: 'https://developers.google.com/maps/'
      }
    });

    var marker = new google.maps.Marker({
      map: map,
      position: saveWidget.getPlace().location
    });

  }



  // count numbers
  $('.timer').countTo({
    speed: 2000,
    refreshInterval: 60,
    formatter: function (value, options) {
      return value.toFixed(options.decimals);
    }
  });

  // Scroll reveal calls
  window.sr = ScrollReveal();
  sr.reveal('.sr-icons', {
    duration: 600,
    scale: 0.3,
    mobile: true,
    reset: true,
    distance: '0px'
  }, 200);





})(jQuery); // End of use strict
